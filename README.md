## React tutorial

This is my personal execution of official React tutorial. It is commented in Finnish and made as an assignment for JAMK course of Web Application Development.

Project contains ready build folder that can be deployed with "npm install -g serve" and "serve -s build" commands.  

Assignment related question:  
Q: How does Reacts immutability-princible appear in the application?  
A: It appears when the program refers to history and squares arrays. Both arrays are sliced instead of just copying, so that they do not contain reference to eachother. This for example makes it possible to keep a history of the actions during the game.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).