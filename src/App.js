import React, { Component } from 'react';
import './App.css';

/*
vanha Square luokka
class Square extends Component { //Huom: Vaihdettu React.Component ihan vain Componentiksi. Turha hakea kauempaa kun on jo importattu tänne.
  /*Constructoria ei enää tarvita koska state tapahtuu boardissa eikä squaressa.
    constructor(props){ //Luodaan konstruktori square luokalle
    super(props); //Kutsutaan super funktiota luotaessa komponentin alaluokkaa. Propsit lisättävä jos niitä haluaa käyttää.
    this.state = {
      value: null,
    };
  }
  render() {
    return (
      /*Annetaan neliölle toiminto hakemalla propsin onClick funktio.
      Funktio tulee hakea arrow syntaksilla, koska muutoin funktio käynnistyisi heti*/
/*      <button className="square" onClick={() => this.props.onClick({value: 'X'})}>
      {this.props.value}
    </button>
  );
}
}  
*/

//Square muutettu functional componentiksi.
//Huomeaa että funktion hakeminen toteutettu jälleen erilaisella syntaksilla, jotta funktio ei laukeaisi samantien.
function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  )
}

class Board extends Component {
  /*constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null), // Annetaan taulukko jonka ruudut vastaavat pelilaudan ruutuja
      xIsNext: true, //Määritellään vuoro boolean
    }
  }*/

  renderSquare(i) {
    return <Square
      value={this.props.squares[i]} //Antaa this.propin ja i arvon squarelle
      onClick={() => this.props.onClick(i)} //Passaa onClick funktion squarelle. Ajatuksena pitää board ja square ajantasalla tilasta.
      />;
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{ // Luodaan historia taulukko joka pitää kirjaa pelin liikkeistä
        squares: Array(9).fill(null),
      }],
      stepNumber: 0, // Kertoo mikä on tämänhetkinen historiavaihe
      xIsNext: true, // Kertoo kumpi pelaaja toimii seuraavaksi
    }
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber +1); // Kopioi historian step +1 asti.
    const current = history[history.length -1];
    const squares = current.squares.slice(); // Tehdään referenssitön kopio staten square muuttujasta slicen avulla
    if(calculateWinner(squares) ||squares[i]) { // Ei ajeta klikkejä jos voittaja on jo löytynyt
      return;
    }
    squares[i] = this.state.xIsNext? 'X' : 'O'; // Asetetaan indeksin mukainen ruutu vuoronomistajan arvoon
    this.setState({
      history: history.concat([{
        squares: squares // Päivitetään staten squares muuttuja.
      }]),
      stepNumber: history.length, // Asetetaan stepNumber viimeisimpään historian indeksiin
      xIsNext: !this.state.xIsNext, // Invertoidaan xIsNext muuttuja
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step, // Vaihdetaan stepNumber haluttuun vaiheeseen
      xIsNext: (step % 2) === 0, // Tehdään tarkistus jakojäännöksellä ja jos jäännös on 0, palauttaa arvon true eli xIsNext = true;
    })
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber]; // Haetaan historia ja otetaan siitä statenumberin mukainen tila
    const winner = calculateWinner(current.squares); // Tarkistetaan voittiko kumpikaan

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move :
        'Go to game start';
      return (
        // Annetaan avain elementille jotta ne voidaan renderöidä oikeassa järjestyksessä
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      )
    })

    let status;
    if(winner === "d"){
      status = "Game over, Tie!";
    } else if(winner) {
      status = 'Winner: ' + winner;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

//Tässä oli aikaisemmin ReactDOM.render funktio, joka löytyy jo index.js tiedostosta joten poistettiin tästä.

//Apufunktio jolla voidaan määritellä voittaja
function calculateWinner(squares) {
  const lines = [ // Määrittelee linjat joita pitkin voitto voi tapahtua
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

 // Käydään läpi kukin linjavaihtoehto ja jos kaikilla linjoilla on sama merkki, palautetaan voittajan tieto  
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a]; //Palauttaa käytännössä X:n tai Y:n.
    }
  }

  // Tarkistetaan jäikö tyhjiä ruutuja laudalle, jos on vielä, annetaan pelin jatkua.
  for(let i = 0; i < squares.length; i++){
    if(squares[i] === null){
      return null;
    }
  }

  // Muutoin ilmoitetaan tasatuloksesta.
  return "d";
}

export default Game;